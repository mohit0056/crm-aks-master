package com.crm.akscrm;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crm.akscrm.Adapter.AttendanceAdapter;
import com.crm.akscrm.Adapter.TargetReport;

import com.crm.akscrm.model.GetTargetReports;
import com.crm.akscrm.utill.ApiURL;
import com.crm.akscrm.utill.NetworkCall;
import com.crm.akscrm.utill.Progress;
import com.google.gson.Gson;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TargetReports extends  AppCompatActivity implements NetworkCall.MyNetworkCallBack, TargetReport.ReturnView{
    Progress progress;
    NetworkCall networkCall;
    SharedPreferences mSharedPreference;
    RecyclerView recyclerView;
    ArrayList<GetTargetReports> arrGetTargetReports = new ArrayList<>();
    TargetReport targetReport;
    GetTargetReports getTargetReports;
    String userid;
    RelativeLayout norecord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_target_reports);

        progress = new Progress(TargetReports.this);
        networkCall = new NetworkCall(TargetReports.this, TargetReports.this);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        userid = (mSharedPreference.getString("id", ""));
        recyclerView = findViewById(R.id.taskRecycler);
        norecord = findViewById(R.id.relativeData);

        getTargetReportss();

    }

    private void getTargetReportss() {
        networkCall.NetworkAPICall(ApiURL.getTargetReport, true);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void back(View view) {
        super.onBackPressed();

    }

    @Override
    public void getAdapterView(View view, List objects, int position, int from) {
        getTargetReports = arrGetTargetReports.get(position);

        TextView target,productId,date;

        target = view.findViewById(R.id.target);
        productId = view.findViewById(R.id.productId);
        date = view.findViewById(R.id.date);

        String mtarget = "", mproductId = "", mdate = "";


        mtarget = getTargetReports.getTarget();
        mproductId = getTargetReports.getProduct_id();
        mdate = getTargetReports.getDate();



        target.setText(mtarget);
        productId.setText(mproductId);
        date.setText(mdate);


    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.getTargetReport:
                ion = (Builders.Any.B) Ion.with(TargetReports.this)
                        .load("POST", ApiURL.getTargetReport)
                        .setHeader("token", "zsd16xzv3jsytnp87tk7ygv73k8zmr0ekh6ly7mxaeyeh46oe8")
                        .setBodyParameter("user_id", userid);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.getTargetReport:
                try {
                    JSONObject getTask = new JSONObject(jsonstring.toString());
                    String status = getTask.getString("success");
                    String msg = getTask.getString("message");

                    if (status.equals("true")) {

                        JSONArray getTaskList = getTask.getJSONArray("data");

                        for (int i = 0; i < getTaskList.length(); i++) {
                            getTargetReports = new Gson().fromJson(getTaskList.optJSONObject(i).toString(), GetTargetReports.class);
                            arrGetTargetReports.add(getTargetReports);


                        }

                        targetReport= new TargetReport(arrGetTargetReports, TargetReports.this, R.layout.layout_attendance_history, this, 1);
                        recyclerView.setLayoutManager(new LinearLayoutManager(TargetReports.this));
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setAdapter(targetReport);
                    } else {

                        Toast.makeText(TargetReports.this, "Data not found", Toast.LENGTH_SHORT).show();
                        progress.dismiss();

                    }
                } catch (JSONException e1) {
                    //     Toast.makeText(AttendanceHistory.this, "Data not found", Toast.LENGTH_SHORT).show();
                    norecord.setVisibility(View.VISIBLE);
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }
}