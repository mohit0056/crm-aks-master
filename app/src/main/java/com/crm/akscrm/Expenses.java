package com.crm.akscrm;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Expenses extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expenses);
    }

    public void submitExpenses(View view) {

        Intent intent = new Intent(Expenses.this, SubmitExpense.class);
        startActivity(intent);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void expenseReport(View view) {
        Intent intent = new Intent(Expenses.this, ExpenseReport.class);
        startActivity(intent);

    }

    public void back(View view) {
        super.onBackPressed();

    }
}