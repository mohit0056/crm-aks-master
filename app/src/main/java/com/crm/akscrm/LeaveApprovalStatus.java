package com.crm.akscrm;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crm.akscrm.Adapter.LeaveApprovalStatusAdapter;
import com.crm.akscrm.Adapter.TaskAdapter;
import com.crm.akscrm.model.GetLeaveApproveStatus;
import com.crm.akscrm.model.GetTask;
import com.crm.akscrm.utill.ApiURL;
import com.crm.akscrm.utill.NetworkCall;
import com.crm.akscrm.utill.Progress;
import com.google.gson.Gson;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LeaveApprovalStatus extends AppCompatActivity implements NetworkCall.MyNetworkCallBack, LeaveApprovalStatusAdapter.ReturnView {
    Progress progress;
    NetworkCall networkCall;
    SharedPreferences mSharedPreference;
    RecyclerView recyclerView;
    RelativeLayout norecord;

    ArrayList<GetLeaveApproveStatus> arrGetLeaveApproveStatus = new ArrayList<>();
    LeaveApprovalStatusAdapter leaveApprovalStatusAdapter;
    GetLeaveApproveStatus model_GetLeaveApproveStatus;
    String userid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_approval_status);

        progress = new Progress(LeaveApprovalStatus.this);
        networkCall = new NetworkCall(LeaveApprovalStatus.this, LeaveApprovalStatus.this);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        userid = (mSharedPreference.getString("id", ""));
        recyclerView = findViewById(R.id.leavestatusApprovalRecycler);
        norecord = findViewById(R.id.relativeData);

        getApprovalStatus();
    }

    private void getApprovalStatus() {
        networkCall.NetworkAPICall(ApiURL.getApprovalStatus, true);

    }

    public void back(View view) {
        super.onBackPressed();

    }


    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.getApprovalStatus:
                ion = (Builders.Any.B) Ion.with(LeaveApprovalStatus.this)
                        .load("POST", ApiURL.getApprovalStatus)
                        .setHeader("token", "zsd16xzv3jsytnp87tk7ygv73k8zmr0ekh6ly7mxaeyeh46oe8")
                        .setBodyParameter("user_id", userid);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.getApprovalStatus:
                try {
                    JSONObject getApprovalStatus = new JSONObject(jsonstring.toString());
                    String status = getApprovalStatus.getString("success");
                    if (status.equals("true")) {

                        JSONArray getApprovalStatusList = getApprovalStatus.getJSONArray("data");

                        for (int i = 0; i < getApprovalStatusList.length(); i++) {
                            model_GetLeaveApproveStatus = new Gson().fromJson(getApprovalStatusList.optJSONObject(i).toString(), GetLeaveApproveStatus.class);
                            arrGetLeaveApproveStatus.add(model_GetLeaveApproveStatus);


                        }

                        leaveApprovalStatusAdapter = new LeaveApprovalStatusAdapter(arrGetLeaveApproveStatus, LeaveApprovalStatus.this, R.layout.layout_leave_approve_status, this, 1);
                        recyclerView.setLayoutManager(new LinearLayoutManager(LeaveApprovalStatus.this));
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setAdapter(leaveApprovalStatusAdapter);
                    } else {

                        Toast.makeText(LeaveApprovalStatus.this, "Data not found", Toast.LENGTH_SHORT).show();
                        progress.dismiss();

                    }
                } catch (JSONException e1) {

                    norecord.setVisibility(View.VISIBLE);
                              }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }

    @Override
    public void getAdapterView(View view, List objects, int position, int from) {

        model_GetLeaveApproveStatus = arrGetLeaveApproveStatus.get(position);

        TextView period, dateFrom, dateTo, type, reasons, date, status;

        period = view.findViewById(R.id.period);
        dateFrom = view.findViewById(R.id.dateFrom);
        dateTo = view.findViewById(R.id.dateTo);
        type = view.findViewById(R.id.type);
        reasons = view.findViewById(R.id.reasons);
        date = view.findViewById(R.id.date);
        status = view.findViewById(R.id.status);

        String myperiod="", mydateFrom="", mydateTo="", mytype="", myreasons="", mydate="", mystatus="";


        myperiod = model_GetLeaveApproveStatus.getPeriod();
        mydateFrom = model_GetLeaveApproveStatus.getDateFrom();
        mydateTo = model_GetLeaveApproveStatus.getDateTo();
        mytype = model_GetLeaveApproveStatus.getType();
        myreasons = model_GetLeaveApproveStatus.getReason();
        mydate = model_GetLeaveApproveStatus.getDate();
        mystatus = model_GetLeaveApproveStatus.getStatus();


        period.setText(myperiod);
        dateFrom.setText(mydateFrom);
        dateTo.setText(mydateTo);
        type.setText(mytype);
        reasons.setText(myreasons);
        date.setText(mydate);
        status.setText(mystatus);



    }

}