package com.crm.akscrm.model;

public class GetLeaveApproveStatus {


    private String user_id;

    private String period;

    private String date;

    private String dateFrom;

    private String dateTo;

    private String type;

    private String reason;

    private String status;

    public void setUser_id(String user_id){
        this.user_id = user_id;
    }
    public String getUser_id(){
        return this.user_id;
    }
    public void setPeriod(String period){
        this.period = period;
    }
    public String getPeriod(){
        return this.period;
    }
    public void setDate(String date){
        this.date = date;
    }
    public String getDate(){
        return this.date;
    }
    public void setDateFrom(String dateFrom){
        this.dateFrom = dateFrom;
    }
    public String getDateFrom(){
        return this.dateFrom;
    }
    public void setDateTo(String dateTo){
        this.dateTo = dateTo;
    }
    public String getDateTo(){
        return this.dateTo;
    }
    public void setType(String type){
        this.type = type;
    }
    public String getType(){
        return this.type;
    }
    public void setReason(String reason){
        this.reason = reason;
    }
    public String getReason(){
        return this.reason;
    }
    public void setStatus(String status){
        this.status = status;
    }
    public String getStatus(){
        return this.status;
    }
}