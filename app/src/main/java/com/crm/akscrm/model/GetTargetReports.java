package com.crm.akscrm.model;

public class GetTargetReports {

    private String id;

    private String user_id;

    private String date;

    private String target;

    private String product_id;

    public void setId(String id){
        this.id = id;
    }
    public String getId(){
        return this.id;
    }
    public void setUser_id(String user_id){
        this.user_id = user_id;
    }
    public String getUser_id(){
        return this.user_id;
    }
    public void setDate(String date){
        this.date = date;
    }
    public String getDate(){
        return this.date;
    }
    public void setTarget(String target){
        this.target = target;
    }
    public String getTarget(){
        return this.target;
    }
    public void setProduct_id(String product_id){
        this.product_id = product_id;
    }
    public String getProduct_id(){
        return this.product_id;
    }
}