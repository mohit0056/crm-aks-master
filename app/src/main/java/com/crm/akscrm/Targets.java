package com.crm.akscrm;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Targets extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_targets);
    }

    public void targets(View view) {

        Intent intent = new Intent(Targets.this, Alltargets.class);
        startActivity(intent);
    }

    public void TargetReports(View view) {

        Intent intent = new Intent(Targets.this, TargetReports.class);
        startActivity(intent);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void back(View view) {
        super.onBackPressed();

    }
}