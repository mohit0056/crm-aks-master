package com.crm.akscrm;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crm.akscrm.utill.ApiURL;
import com.crm.akscrm.utill.GPSTracker;
import com.crm.akscrm.utill.NetworkCall;
import com.crm.akscrm.utill.Progress;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Attendance extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {
    private GoogleMap mMap;
    TextView time, date, location;
    String mydatePunchin, mydatePunchout, myPunchInTime, myPunchOutTime, getLocation;
    RelativeLayout punchIn, punchOut, getMylocation;
    FusedLocationProviderClient client;
    LinearLayout viewAttendance;
    SupportMapFragment mapFragment;
    String FullAddressIn = "", userid, Filladdressout = "", addressIn = "", addressOut = "", dateTime = "", mlocaltxt = "", mcitytxt = "", mstatetxt = "", mpintxt = "";
    SharedPreferences mSharedPreference;
    boolean cancle = false;
    FusedLocationProviderClient fusedLocationProviderClient;
    Button btLocation;
    String date_loginin = "", date1 = "", date_loginout = "",currenttime;

    NetworkCall networkCall;
    Progress progress;
    GPSTracker gps;
    double latitude, longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        userid = (mSharedPreference.getString("id", ""));
        statusCheck();

        progress = new Progress(Attendance.this);
        networkCall = new NetworkCall(Attendance.this, Attendance.this);

        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        addressIn = (mSharedPreference.getString("fulladdressIn", ""));
        addressOut = (mSharedPreference.getString("fulladdressOut", ""));
        dateTime = (mSharedPreference.getString("dateAndTimein", ""));
        myPunchInTime = (mSharedPreference.getString("datein", ""));
        myPunchOutTime = (mSharedPreference.getString("dateout", ""));
        userid = (mSharedPreference.getString("id", ""));
        date_loginin = (mSharedPreference.getString("date_loginin", ""));
        date_loginout = (mSharedPreference.getString("date_loginout", ""));


        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy  HH:mm:ss", Locale.getDefault());

        currenttime = sdf.format(new Date());
        mydatePunchin = sdf.format(new Date());

        String[] splited = mydatePunchin.split("\\s+");
        date1 = splited[0];
        String splitlogin_time = splited[1];


        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        client = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(Attendance.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            getcurrentLocation();
        } else {
            ActivityCompat.requestPermissions(Attendance.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);
        }


        punchIn = findViewById(R.id.punchIn);
        punchIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(Attendance.this,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(Attendance.this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                }
//                getLocationnIn();
                getcurrent_locationin();

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy  HH:mm:ss", Locale.getDefault());
                mydatePunchin = sdf.format(new Date());
                SharedPreferences.Editor editor = mSharedPreference.edit();
                editor.putString("datein", mydatePunchin);
                editor.apply();
                PunchIn();


            }
        });


        punchOut = findViewById(R.id.punchOut);
        punchOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(Attendance.this,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(Attendance.this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                }
                getcurrent_locationout();
//                getLocationnout();
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy  HH:mm:ss", Locale.getDefault());
                mydatePunchout = sdf1.format(new Date());
                SharedPreferences.Editor editor = mSharedPreference.edit();
                editor.putString("dateout", mydatePunchout);
                editor.apply();
                Punchout();
            }
        });
        time = findViewById(R.id.time);
        date = findViewById(R.id.date);
        date.setText(currenttime);



    }


    private void getcurrentLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Task<Location> task = client.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    mapFragment.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(GoogleMap googleMap) {
                            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                            getLocation = String.valueOf(new LatLng(location.getLatitude(), location.getLongitude()));
                            MarkerOptions options = new MarkerOptions().position(latLng).title("Im here");

                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18));
                            googleMap.addMarker(options);

                        }
                    });
                }
            }
        });
    }

    private void getcurrent_locationin() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Attendance.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
//            Toast.makeText(getApplicationContext(),"You need have granted permission",Toast.LENGTH_SHORT).show();
            gps = new GPSTracker(Attendance.this);
            // Check if GPS enabled
            if (gps.canGetLocation()) {
                latitude = gps.getLatitude();
                longitude = gps.getLongitude();
//                Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                try {
                    Geocoder geo = new Geocoder(Attendance.this.getApplicationContext(), Locale.getDefault());
                    List<Address> addresses = geo.getFromLocation(latitude, longitude, 1);
                    if (addresses.isEmpty()) {
//                        customeraddress.setText("Waiting for Location");
                    } else {
                        if (addresses.size() > 0) {
                            FullAddressIn = addresses.get(0).getFeatureName() + ", " + addresses.get(0).getLocality() + ", " + addresses.get(0).getAdminArea() + ", " + addresses.get(0).getCountryName();
                            SharedPreferences.Editor editor = mSharedPreference.edit();
                            editor.putString("fulladdressIn", FullAddressIn);
                            editor.apply();

//                            customeraddress.setText(maddress);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace(); // getFromLocation() may sometimes fail
                }
            } else {
                gps.showSettingsAlert();
            }
        }
    }

    private void getcurrent_locationout() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Attendance.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
//            Toast.makeText(getApplicationContext(),"You need have granted permission",Toast.LENGTH_SHORT).show();
            gps = new GPSTracker(Attendance.this);
            // Check if GPS enabled
            if (gps.canGetLocation()) {
                latitude = gps.getLatitude();
                longitude = gps.getLongitude();
//                Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                try {
                    Geocoder geo = new Geocoder(Attendance.this.getApplicationContext(), Locale.getDefault());
                    List<Address> addresses = geo.getFromLocation(latitude, longitude, 1);
                    if (addresses.isEmpty()) {
//                        customeraddress.setText("Waiting for Location");
                    } else {
                        if (addresses.size() > 0) {
                            Filladdressout = addresses.get(0).getFeatureName() + ", " + addresses.get(0).getLocality() + ", " + addresses.get(0).getAdminArea() + ", " + addresses.get(0).getCountryName();
                            SharedPreferences.Editor editor = mSharedPreference.edit();
                            editor.putString("fulladdressOut", Filladdressout);
                            editor.apply();


//                            customeraddress.setText(maddress);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace(); // getFromLocation() may sometimes fail
                }
            } else {
                gps.showSettingsAlert();
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the

                    // contacts-related task you need to do.

                    gps = new GPSTracker(getApplicationContext());

                    // Check if GPS enabled
                    if (gps.canGetLocation()) {

                        latitude = gps.getLatitude();
                        longitude = gps.getLongitude();

                        Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                    } else {
                        gps.showSettingsAlert();
                    }

                } else {
//                    gps.showSettingsAlert();
                    Toast.makeText(getApplicationContext(), "You need to grant permission", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    public void attendanceHistory(View view) {

        Intent intent = new Intent(Attendance.this, AttendanceHistory.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void back(View view) {
        super.onBackPressed();

    }

    private void PunchIn() {
        networkCall.NetworkAPICall(ApiURL.userPunchIn, true);

    }

    private void Punchout() {
        networkCall.NetworkAPICall(ApiURL.userPunchOut, true);

    }

//    private void Punchout() {
//        networkCall.NetworkAPICall(ApiURL.user_punch_out, true);
//
//    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.userPunchIn:
                ion = (Builders.Any.B) Ion.with(Attendance.this)
                        .load("POST", ApiURL.userPunchIn)
                        .setHeader("token", "zsd16xzv3jsytnp87tk7ygv73k8zmr0ekh6ly7mxaeyeh46oe8")
                        .setBodyParameter("user_id", userid)
                        .setBodyParameter("punchintime", mydatePunchin)
                        .setBodyParameter("punchinlocation", FullAddressIn);
                break;

            case ApiURL.userPunchOut:
                ion = (Builders.Any.B) Ion.with(Attendance.this)
                        .load("POST", ApiURL.userPunchOut)
                        .setHeader("token", "zsd16xzv3jsytnp87tk7ygv73k8zmr0ekh6ly7mxaeyeh46oe8")
                        .setBodyParameter("user_id", userid)
                        .setBodyParameter("punchouttime", mydatePunchout)
                        .setBodyParameter("punchoutlocation", Filladdressout);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.userPunchIn:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String status = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");
                    if (status.equals("true")) {
                        date.setText(mydatePunchin);

//                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
//                        String login_time = jsonObject1.getString("login_time");
//                        String[] splited = login_time.split("\\s+");
//                        date_loginin=splited[0];
//                        String splitlogin_time=splited[1];
//
//                        Log.e("splitlogin_time", String.valueOf(splitlogin_time));

                        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
//                        punchIn.setVisibility(View.GONE);
//                        punchOut.setVisibility(View.VISIBLE);
//                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//                        SharedPreferences.Editor editor = prefs.edit();
//                        editor.putString("date_loginin", date_loginin);
//                        editor.apply();
                    } else {


                        String fail_status = jsonObject.getString("success");

                        if (fail_status.equals("false")) {

                            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();


                            //  Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                        }
                        progress.dismiss();
                    }
                } catch (JSONException e1) {

                    Toast.makeText(this, "Something Went Wrong...", Toast.LENGTH_SHORT).show();
                }

                break;

            case ApiURL.userPunchOut:
                try {
                    JSONObject jsonObjectout = new JSONObject(jsonstring.toString());
                    String status = jsonObjectout.getString("success");
                    String msg = jsonObjectout.getString("message");
                    if (status.equals("true")) {
                        date.setText(mydatePunchout);

//                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
//                        String login_time = jsonObject1.getString("login_time");
//                        String[] splited = login_time.split("\\s+");
//                        date_loginin=splited[0];
//                        String splitlogin_time=splited[1];
//
//                        Log.e("splitlogin_time", String.valueOf(splitlogin_time));

                        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
//                        punchIn.setVisibility(View.GONE);
//                        punchOut.setVisibility(View.VISIBLE);
//                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//                        SharedPreferences.Editor editor = prefs.edit();
//                        editor.putString("date_loginin", date_loginin);
//                        editor.apply();
                    } else {


                        String fail_status = jsonObjectout.getString("success");

                        if (fail_status.equals("false")) {

                            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();


                            //  Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                        }
                        progress.dismiss();
                    }
                } catch (JSONException e1) {

                    Toast.makeText(this, "Something Went Wrong...", Toast.LENGTH_SHORT).show();
                }

                break;
        }

    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your Location seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}